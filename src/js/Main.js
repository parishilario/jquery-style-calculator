$(document).ready(function() {
	let expression = "";
	let operators = ["+", "-", "*", "/"];
	let opRegex = /\W$/;
	let deciRegex = /\./;
	$(".value").click(function() {
		if(expression.length > 15) {
			alert("Warning! Characters exceed 15 limit.");
		} else if(expression.search(opRegex) > -1 && operators.includes($(this).html())) {
		    expression = expression.replace(expression.substr(expression.length - 1, 1), $(this).html());
			$("#box").val(expression);
		} else {
			expression += $(this).html();
			$("#box").val(expression);
		}
	});
});